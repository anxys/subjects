module Instances where

import           Test.QuickCheck

data ChessPosition = ChessPosition Int Int
  deriving (Show, Eq)

instance Arbitrary ChessPosition where
  arbitrary = do
    x <- choose (1, 8)
    y <- choose (1, 8)
    return (ChessPosition x y)
