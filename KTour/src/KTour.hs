module KTour where
import           Control.Monad.State
import           Data.Char           (chr, ord)
import           Data.List
import           Data.Ord            (comparing)
import           Test.QuickCheck

data ChessPosition = ChessPosition Int Int
  deriving (Show, Eq)

bSize :: Int
bSize = 8

board :: Int -> [ChessPosition]
board size = [ChessPosition x y | x <- [ 1..size ], y <- [ 1..size ] ]

availableMoves :: ChessPosition -> [ChessPosition]
availableMoves (ChessPosition x y) = filter (`elem` board bSize) ls
  where v  = [ 1, -1, 2, -2 ]
        ls = [ ChessPosition (x+i) (y+j) | i <- v, j <- v, abs i /= abs j ]

knightTour :: ChessPosition -> [ChessPosition]
knightTour pos = knightTour' [pos]

knightTour' :: [ChessPosition] -> [ChessPosition]
knightTour' [] = []
knightTour' moves@(currPos:_)
  -- It has been proven that for any board larger than (or equal to ) 5 by 5 
  -- there is a solution for every starting position
  | null possibleMoves = reverse moves   
  | otherwise = knightTour' $ nextMove : moves
  where
    findMoves s = availableMoves s \\ moves
    possibleMoves = findMoves currPos
    nextMove = minimumBy (comparing (length . findMoves)) possibleMoves

-----------------------------------------------------------------------------
--                         Tests
-----------------------------------------------------------------------------


{-# ANN genChessPosition "TestSupport" #-}
genChessPosition = do
    x <- choose (1, 8)
    y <- choose (1, 8)
    return (ChessPosition x y)

instance Arbitrary ChessPosition where
  arbitrary = genChessPosition 

{-# ANN prop_notNullAvailableMoves "Test" #-}
prop_notNullAvailableMoves x = (not.null) (availableMoves x)

{-# ANN prop_numberOfMovesAvailableMoves "Test" #-}
prop_numberOfMovesAvailableMoves x = 
  all (\(ChessPosition y z) -> y `elem` range && z `elem` range) (availableMoves x)
  where range = [2..8]

{-# ANN prop_boardSizeCorrect "Test" #-}
prop_boardSizeCorrect (Positive x) = length (board x) == x^2

-- The above properties were added later

{-# ANN prop_availableMovesOnBoard "Test" #-}
prop_availableMovesOnBoard s = all isOnBoard (availableMoves s)

{-# ANN prop_length "Test" #-}
prop_length s = length path == 8*8
  where
    path = knightTour s

{-# ANN prop_unique "Test" #-}
prop_unique s = nub path == path
  where
    path = knightTour s

{-# ANN prop_allOnBoard "Test" #-}
prop_allOnBoard s = all isOnBoard path
  where
    path = knightTour s

{-# ANN isOnBoard "TestSupport" #-}
isOnBoard (ChessPosition x y) = onBoard x && onBoard y
  where
    onBoard z = z >= 1 && z <= 8
