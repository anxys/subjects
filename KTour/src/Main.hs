module Main where

import Test.QuickCheck
import KTour

main :: IO ()
main = do
  quickCheckResult prop_allOnBoard
  quickCheckResult prop_boardSizeCorrect
  quickCheckResult prop_availableMovesOnBoard
  quickCheckResult prop_length
  quickCheckResult prop_unique
  return ()
