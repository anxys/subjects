module Triangles where

import           Test.HUnit

-- Define the different types of triangles

data Shape = NoTriangle
           | Equilateral
           | Isosceles
           | Rectangular
           | Other
  deriving (Eq, Show)

-- Triangle stuff

triangle :: Integer -> Integer -> Integer -> Shape
triangle x y z
  | not (x + z >= y && x + y >= z && z + y >= x) || x == 0 || y == 0 || z == 0  =  NoTriangle
  | x == y && x == z                                                            =  Equilateral
  | x == y || x == z || y == z                                                  =  Isosceles
  | x ^ 2 + y ^ 2 == z ^ 2 || x ^ 2 + z ^ 2 == y ^ 2 || y ^ 2 + z ^ 2 == x ^ 2  =  Rectangular
  | otherwise                                                                   =  Other

--------------------------------------------------------------------------------
-------------------------- Tests
--------------------------------------------------------------------------------

{-# ANN pythagoreanTriples "TestSupport" #-}
pythagoreanTriples :: Integer -> [(Integer, Integer, Integer)]
pythagoreanTriples n = [(x, y, z) | x <- [1 .. n]
                                  , y <- [1 .. n]
                                  , z <- [1 .. n]
                                  , x <= y
                                  , (x ^ 2) + (y ^ 2) == (z ^ 2)]

{-# ANN testRightTriangle "TestSupport" #-}
testRightTriangle :: Bool
testRightTriangle = all (\(x, y, z) -> triangle x y z == Rectangular) (pythagoreanTriples 50)

{-# ANN test1 "Test" #-}
test1 = TestCase (assertBool "Rectangular" testRightTriangle)
{-# ANN test2 "Test" #-}
test2 = TestCase (assertEqual "Isosceles" (triangle 6 6 4) Isosceles)
{-# ANN test3 "Test" #-}
test3 = TestCase (assertEqual "NoTriangle" (triangle 0 1 2) NoTriangle)
{-# ANN test4 "Test" #-}
test4 = TestCase (assertEqual "Equilateral" (triangle 4 4 4) Equilateral)

{-# ANN tests "TestSupport" #-}
tests = TestList
          [ TestLabel "Test 1" test1
          , TestLabel "Test 2" test2
          , TestLabel "Test 3" test3
          , TestLabel "Test 4" test4
          ]
