{-# LANGUAGE DeriveDataTypeable   #-}
module Language.SimpleTypes where

import           Data.Data
import           Data.Generics.Uniplate.Data

data FieldType
  = Money
  | Integer
  | String
  | Boolean
  deriving (Eq,Show,Typeable,Data)
