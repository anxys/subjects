module Language.Syntax.Parser
       (Language.Syntax.Parser.parse, form, expr, ifStmnt, ifElseStmnt,
        conditional, simpleField, calculatedField, ParseError)
       where
import Test.Hspec
import Data.Either
import Control.Monad
import Language.Identifier
import qualified Language.Syntax.Simple.Ast as SA
import Language.Syntax.Simple.Simplify (simplify)
import Language.Syntax.Ast as A
import Language.Syntax.Parser.Extended
import Language.Location
import Text.ParserCombinators.Parsec as P hiding (label)
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

languageDef :: LanguageDef st
languageDef
  = emptyDef{Token.commentStart = "/*", Token.commentEnd = "*/",
             Token.commentLine = "//", Token.identStart = letter <|> char '_',
             Token.identLetter = alphaNum,
             Token.reservedNames =
               ["if", "else", "form", "true", "false", "not", "and", "or",
                "integer", "string", "boolean"],
             Token.reservedOpNames =
               ["+", "-", "*", "/", "and", "or", "not", ":", "++", ">", "<", "==",
                "!=", ">=", "<="],
             Token.caseSensitive = True}

lexer :: Token.TokenParser st
lexer = Token.makeTokenParser languageDef

identifier :: Parser String
identifier = Token.identifier lexer

reserved :: String -> Parser ()
reserved = Token.reserved lexer

reservedOp :: String -> Parser ()
reservedOp = Token.reservedOp lexer

parens :: Parser a -> Parser a
parens = Token.parens lexer

braces :: Parser a -> Parser a
braces = Token.braces lexer

integer :: Parser Integer
integer = Token.integer lexer

stringLiteral :: Parser String
stringLiteral = Token.stringLiteral lexer

integerT :: Parser (FieldType Location)
integerT = addLoc0 Integer (reserved "integer") <?> "integer type"

boolT :: Parser (FieldType Location)
boolT = addLoc0 Boolean (reserved "boolean") <?> "bool type"

stringT :: Parser (FieldType Location)
stringT = addLoc0 String (reserved "string") <?> "string type"

fieldT :: Parser (FieldType Location)
fieldT = integerT <|> stringT <|> boolT <?> "field type"

field :: Parser (Statement Location)
field = addLoc1 Field (P.try calculatedField <|> simpleField)

fieldInfo :: Parser (FieldInformation Location)
fieldInfo
  = do text <- stringLiteral
       name <- identifier
       reserved ":"
       t <- fieldT
       return (FieldInformation text (mkIdentifier name) t)

simpleField :: Parser (Field Location)
simpleField = addLoc1 SimpleField fieldInfo <?> "simple field"

calculatedField :: Parser (Field Location)
calculatedField
  = addLoc2 CalculatedField (fieldInfo <* reserved "=") expr <?>
      "calculated field"

block :: Parser (Block Location)
block = braces (many stmnt) <?> "block"

expr :: Parser (Expression Location)
expr = buildExpressionParser opTable term <?> "expression"

binop_ ::
       Location ->
         (Location -> BinaryOperation Location) ->
           Expression Location -> Expression Location -> Expression Location
binop_ l op = BinaryOperation l (op l)

uop_ ::
     Location ->
       (Location -> UnaryOperation Location) ->
         Expression Location -> Expression Location
uop_ l op = UnaryOperation l (op l)

opTable :: [[Operator Char () (Expression Location)]]
opTable
  = [[unary "not" A.Not],
     [binary ">=" A.GreaterThanOrEquals AssocLeft,
      binary "" A.LesserThanOrEquals AssocLeft,
      binary ">" A.GreaterThan AssocLeft,
      binary "<" A.LesserThan AssocLeft],
     [binary "==" A.Equals AssocLeft,
      binary "!=" A.NotEquals AssocLeft],
     [binary "and" A.And AssocLeft, binary "or" A.Or AssocLeft],
     [binary "*" A.Multiplication AssocLeft,
      binary "/" A.Division AssocLeft],
     [binary "+" A.Addition AssocLeft,
      binary "-" A.Subtraction AssocLeft],
     [binary "++" A.StringConcatenation AssocLeft]]
  where binary name op
          = Infix
              (do loc <- opLocation (reservedOp name)
                  return (binop_ loc op))
        unary name op
          = Prefix
              (do loc <- opLocation (reservedOp name)
                  return (uop_ loc op))

opLocation :: Parser a -> Parser Location
opLocation p
  = do s <- getPosition
       _ <- p
       e <- getPosition
       return (newLoc s e)

var :: Parser (Expression Location)
var
  = addLoc1 Variable (liftM mkIdentifier identifier) <?> "variable"

term :: Parser (Expression Location)
term = parens expr <|> var <|> lit <?> "term"

lit :: Parser (Expression Location)
lit = addLoc1 Literal (slit <|> ilit <|> blit) <?> "literal"

slit :: Parser (Literal Location)
slit = addLoc1 StringLiteral stringLiteral <?> "string literal"

ilit :: Parser (Literal Location)
ilit = addLoc1 IntegerLiteral integer <?> "integer literal"

blit :: Parser (Literal Location)
blit = addLoc1 BooleanLiteral (true <|> false) <?> "bool literal"
  where true = reserved "true" >> return True
        false = reserved "false" >> return False

stmnt :: Parser (Statement Location)
stmnt = conditional <|> field <?> "statement"

conditional :: Parser (Statement Location)
conditional = P.try ifElseStmnt <|> ifStmnt <?> "conditional"

if' :: Parser (Expression Location, Block Location)
if'
  = do reserved "if"
       cond <- parens expr
       body <- block
       return (cond, body)

ifStmnt :: Parser (Statement Location)
ifStmnt
  = do s <- getPosition
       (cond, body) <- if'
       e <- getPosition
       return (If (newLoc s e) cond body) <?> "if statement"

ifElseStmnt :: Parser (Statement Location)
ifElseStmnt
  = do s <- getPosition
       (cond, firstBody) <- if'
       reserved "else"
       secondBody <- block
       e <- getPosition
       return (IfElse (newLoc s e) cond firstBody secondBody) <?>
         "if else statement"

form :: Parser (Form Location)
form
  = addLoc2 Form (reserved "form" *> liftM mkIdentifier identifier)
      (block <* eof)
      <?> "form"

parse :: String -> String -> Either ParseError (Form Location)
parse = P.parse form

testParseForm :: String -> String

testParser :: Parser a -> String -> Either ParseError a

getRight :: Either a b -> b

canParse :: Either a b -> Bool

{-# ANN testParseForm "TestSupport" #-}
testParseForm input
  = case P.parse form "ql" input of
        Left err -> "Error: " ++ show err
        Right val -> show $ simplify val

{-# ANN testParser "TestSupport" #-}
testParser p = P.parse p "ql"

{-# ANN getRight "TestSupport" #-}
getRight (Right x) = x
getRight (Left _)
  = error "Function should have returned a Right value"

{-# ANN canParse "TestSupport" #-}
canParse = isRight

{-# ANN formSpec "Test" #-}
formSpec
  = describe "Parsing Forms" $
      do it "can parse an empty form" $
           testParseForm "form name { }" `shouldBe`
             show (SA.Form (mkIdentifier "name") [])
         it "can parse a Form with two fields " $
           testParseForm
             "form taxOfficeExample { \"Display Text One\" idTest1: integer \"DisplayText2\" \t idTest2: integer}"
             `shouldBe`
             show
               (SA.Form (mkIdentifier "taxOfficeExample")
                  [SA.Field
                     (SA.SimpField
                        SA.FieldInfo{SA.label = "Display Text One",
                                     SA.id = mkIdentifier "idTest1", SA.fieldType = SA.Money}),
                   SA.Field
                     (SA.SimpField
                        SA.FieldInfo{SA.label = "DisplayText2",
                                     SA.id = mkIdentifier "idTest2", SA.fieldType = SA.Integer})])
         it "can parse a Form with an if statement" $
           simplify (getRight (testParser form "form name { if (false) {} }"))
             `shouldBe`
             SA.Form (mkIdentifier "name") [SA.If (SA.Lit (SA.BLit False)) []]
         it "can parse a Form with an if statement containing fields" $
           testParser form
             "form name { if (false) {\"Display Text One\" idTest1: integer} }"
             `shouldSatisfy` canParse

{-# ANN exprSpec "Test" #-}
exprSpec
  = describe "Parsing Expressions" $
      do it "can parse an addition expression" $
           testParser expr "1 + 2" `shouldSatisfy` canParse
         it "can parse a GT comparison" $
           testParser expr "1 > 2" `shouldSatisfy` canParse
         it "can parse a LT comparison" $
           testParser expr "1 < 2" `shouldSatisfy` canParse
         it "can parse an and expression" $
           testParser expr "true and false" `shouldSatisfy` canParse
         it "can parse an or expression" $
           testParser expr "true or false" `shouldSatisfy` canParse
         it "can parse a literal" $
           testParser expr "1" `shouldSatisfy` canParse
         it "can parse a var addition" $
           testParser expr "testValue + 2" `shouldSatisfy` canParse
         it "can parse string literal" $
           testParser expr "\"TestString\"" `shouldSatisfy` canParse

{-# ANN controlSpec "Test" #-}
controlSpec
  = describe "Parsing control flow" $
      do it "can parse an if statement" $
           testParser ifStmnt "if(false) {}" `shouldSatisfy` canParse
         it "can parse an if else statement" $
           testParser ifElseStmnt "if(true) {} else {}" `shouldSatisfy`
             canParse
         it "can parse control flow " $
           testParser conditional "if(true) {} else {}" `shouldSatisfy`
             canParse
         it "can parse control flow" $
           testParser conditional "if(true) {\n}" `shouldSatisfy` canParse

{-# ANN fieldSpec "Test" #-}
fieldSpec
  = describe "Parsing fields" $
      do it "can parse a simple field" $
           testParser simpleField "\"Display Text One\" idTest1: integer"
             `shouldSatisfy` canParse
         it "can parse a calculated field" $
           testParser calculatedField
             "\"Display Text One\" idTest1: integer = 10"
             `shouldSatisfy` canParse
         it "can parse a calculated field with a boolean condition" $
           testParser calculatedField
             "\"Display Text One\" idTest1: boolean = true or true"
             `shouldSatisfy` canParse
         it "can parse a calculated field with a money addition" $
           testParser calculatedField
             "\"Display Text One\" idTest1: integer = 11 + 12"
             `shouldSatisfy` canParse
         it
           "can parse a calculated field with a money addition including a variable identifier"
           $
           testParser calculatedField
             "\"Display Text One\" idTest1: integer = testVar + 12"
             `shouldSatisfy` canParse
